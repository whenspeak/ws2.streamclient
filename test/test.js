var { remote } = require('webdriverio');
var WebSocket = require('ws')
var child_process = require('child_process');

this.countBrowser = 0
this.browsers = []
this.counterTest = 0;
this.intervalsScreenshot = []

child_process.execSync('mkdir -p ./screenshots');  

this.makeid = (val) => {
  let text = "";
  let possible = "abcdef0123456789";

  for(let i = 0; i < val; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

this.clientId = this.makeid(32);
var socket = new WebSocket("ws://37.228.89.170:3010/");

socket.onopen = () => {
  console.log("Соединение установлено.", JSON.stringify({clientId: this.clientId, server: true}));
  socket.send(JSON.stringify({clientId: this.clientId, server: true}))
};

socket.onclose = function(event) {
  if (event.wasClean) {
    console.log('Соединение закрыто чисто');
  } else {
    console.log('Обрыв соединения');
  }
  destroyAll()
  console.log('Код: ' + event.code + ' причина: ' + event.reason);
};

socket.onmessage = (event) => {
    console.log("Получены данные " + event.data);
    let data = JSON.parse(event.data)
    if(data.add) {
       addTest()
    }
    if(data.remove && this.browsers.length > 0) {
       removeTest(0)
    }  
    if(data.removeAll) {
        destroyAll()
     }      
};

destroyAll = () => {
    let length = this.browsers.length;
    for(let i = 0; i < length; i++) {
        removeTest(0)
    }
}

socket.onerror = function(error) {
  console.log("Ошибка " + error.message);
};

screenshot = (browser, position) => {
    let count = 0;
    browser.saveScreenshot(`./screenshots/screenshot${position}/screenshot${count}.png`);  
    this.intervalsScreenshot[position] = null;
    this.intervalsScreenshot[position] = setInterval(() => {
        count++;
        browser.saveScreenshot(`./screenshots/screenshot${position}/screenshot${count}.png`);            
    }, 10000)    
}

destroyAllScreenshots = () => {
    for(let i = 0; i < this.intervalsScreenshot.length; i++) {
        destroyScreenshot(i)
    }
}

destroyScreenshot = (position) => {
    try {
        clearInterval(this.intervalsScreenshot[position])
        this.intervalsScreenshot[position] = null;     
    } catch (error) {   
    }
}

addTest = async () => {
    let brwsr = await remote({
        logLevel: 'error',
        path: '/',
        hostname: 'localhost',
        port: 9515,
        capabilities: {
            acceptSslCerts: true,
            browserName: 'chrome'
        }
    })
    this.browsers.push({
        browser: brwsr,
        counterTest: this.counterTest++
    })
    
    let currentBrowser = this.countBrowser++

    child_process.execSync(`mkdir -p ./screenshots/screenshot${this.browsers[currentBrowser].counterTest}`);     
    try {
        // await this.browsers[currentBrowser].browser.url('https://demostream.whenspeak.ru/public/index.html?server=wss%3A%2F%2Fstream.whenspeak.ru%3A9081%2F&type=viewer&autoplay=false')
        await this.browsers[currentBrowser].browser.url('https://localhost:3031/index.html?type=viewer&autoplay=false')
        await this.browsers[currentBrowser].browser.setTimeout({ 'pageLoad': 10000 });
        await this.browsers[currentBrowser].browser.setTimeout({ 'script': 60000 });
        await this.browsers[currentBrowser].browser.setTimeout({ 'implicit': 5000 });
        await this.browsers[currentBrowser].browser.call(() => {
            screenshot(this.browsers[currentBrowser].browser, this.browsers[currentBrowser].counterTest)    
        })      
        var playButton = await this.browsers[currentBrowser].browser.$('.wspeak-area')  
        await playButton.click()     
    } catch (error) {
        removeTest(currentBrowser)
        socket.send(JSON.stringify({clientId: this.clientId, countTests: this.countBrowser}))
        socket.send(JSON.stringify({clientId: this.clientId, error}))
    }

    
}

addTest()

removeTest = async (position) => {
    let browser = this.browsers[position];
    if(browser && browser.browser && browser.countTests) {
        this.browsers.splice(position, 1)
        this.countBrowser--;
        destroyScreenshot(browser.counterTest)        
        await browser.browser.deleteSession()
        browser.browser = null;
    }


}

