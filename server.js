const path = require('path'),
    express = require('express'),
    https = require('https'),
    fs = require('fs'),
    webpack = require('webpack'),
    webpackConfig = require('./webpack.config.js'),
    app = express(),
    port = process.env.PORT || 3031;



app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './public', 'index.html'))
});


//https
// https.createServer({
//   key: fs.readFileSync('./key/server.key'),
//   cert: fs.readFileSync('./key/server.cert')
// }, app).listen(port, () => { console.log(`App is listening on port ${port}`) });

//http
app.listen(port, () => { console.log(`App is listening on port ${port}`) });

let compiler = webpack(webpackConfig);
app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true, publicPath: webpackConfig.output.publicPath, stats: { colors: true }
}));
app.use(require('webpack-hot-middleware')(compiler));
app.use(express.static(path.resolve(__dirname, './public')));