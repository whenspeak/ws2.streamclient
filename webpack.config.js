const path = require('path'),
    webpack = require('webpack'),
    HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        app: './src/app/index.ts',
    },
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/player.js',
        library: 'whenspeak',
        libraryTarget: 'umd'        
    },
    devtool: 'source-map',
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx']
    },
    module: {
        rules: [
            {
                test: /fonts\.css$/,
                loaders: ['css-loader', 'postcss-loader'],
                include: path.resolve(__dirname, 'src/components/core/public')
            },
            {
                test: /\.scss$/,
                loaders: ['style-loader?singleton=true', 'css-loader', 'postcss-loader', 'sass-loader?includePaths[]='
                    + path.resolve(__dirname, './src/base/scss')
                ],
                include: path.resolve(__dirname, 'src')
            },        
            {
                test: /\.(ts|tsx)$/,
                loader: 'ts-loader'
            },

            { test: /\.(html|svg)$/, loader: 'raw-loader' },
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
}