
/* global URL */
import * as Media from './media'

function setupDefaultValues (options: IAutoplayOptions) {
  return Object.assign({
    muted: false,
    timeout: 250,
    inline: false
  }, options)
}

interface IAutoplayOptions {
    muted: boolean;
    timeout: number;
    inline: boolean;
}

function startPlayback (options: IAutoplayOptions, elementCallback: () => {element: HTMLVideoElement, source: string}) {
  let {element, source} = elementCallback()
  let playResult: any;
  let timeoutId: any;
  let sendOutput: (result: boolean, error: any) => void;

  element.muted = options.muted
  if (options.muted === true) {
    element.setAttribute('muted', 'muted')
  }
  // indicates that the video is to be played "inline",
  // that is within the element's playback area.
  if (options.inline === true) {
    element.setAttribute('playsinline', 'playsinline')
  }

  element.src = source

  return new Promise((resolve) => {
    playResult = element.play()
    timeoutId = setTimeout(() => {
      sendOutput(false, new Error(`Timeout ${options.timeout} ms has been reached`))
    }, options.timeout)
    sendOutput = (result: boolean, error: any = null) => {
      clearTimeout(timeoutId)
      resolve({result, error})
    }

    if(playResult !== undefined) {
      playResult
        .then(() => sendOutput(true, null))
        .catch((playError: any) => sendOutput(false, playError))
    } else {
      sendOutput(true, null)
    }
  })
}

//
// API
//

export function video (options?: IAutoplayOptions) {
  options = setupDefaultValues(options)
  return startPlayback(options, () => {
    return {
      element: document.createElement('video'),
      source: URL.createObjectURL(Media.VIDEO)
    }
  })
}

// export default {video}