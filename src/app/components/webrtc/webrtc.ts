//import {is_safary} from "./utils";
import '../../style/style.scss';
import { IWebRTC } from './IWebRTC';
import { IConfig } from '../IConfig';
import { EventEmitter } from 'eventemitter3';

export class WebRTC extends EventEmitter implements IWebRTC {
  public ws: WebSocket;
  public webRtcPeer: any;
  public el: any;
  public video: HTMLVideoElement;
  public options: IConfig;
  private sources: any[];
  private timeReconnect: number = 5000;
  private reconnectCounter: number = 0;


  constructor(options: IConfig) {
    super()
    this.options = options;
    this.onIceCandidate = this.onIceCandidate.bind(this);
    this.onMessage = this.onMessage.bind(this);
    this.onOffer = this.onOffer.bind(this);
    this.response = this.response.bind(this);

    window.addEventListener("beforeunload", () => {
      this.stop();
    });
    this.initDOM()
    // this.play()     
  }


  initDOM() {
    //создаем контейнер
    let el = document.createElement("div");
    el.className = 'container'

    //создаем видеотег
    let video = document.createElement("video");
    video.setAttribute("autoplay", "");
    video.setAttribute("playsinline", "");
    // video.setAttribute("muted", "");
    video.setAttribute("poster", "img/webrtc.png");
    //video.setAttribute("controls","");
    el.appendChild(video);
    this.options.element.appendChild(el)
    this.el = el;
    this.video = video;
    this.video.addEventListener('playing', () => {
      this.timeReconnect = 5000;
      this.reconnectCounter = 0;
      this.emit('playing')
    })
  }


  onMessage(message: any){
    var parsedMessage = JSON.parse(message.data);
    console.info('Received message: ' + message.data);
    switch (parsedMessage.event) {
      case 'recordPaused':
        break;
      case 'reconnectClient':
        this.reconnect();
        break;              
      case 'viewerResponse':
        this.response(parsedMessage);
        break;      
      case 'presenterResponse':
        this.response(parsedMessage);
        break;
      case 'stopCommunication':
        this.dispose();
        break;
      case 'updateSources':
        this.dispatchSources(parsedMessage.payload);
        break;
      case 'updateVideoRecords':
        this.dispatchVideoUrls(parsedMessage.payload);
        break;
      case 'recordStarted':
        this.emit('record_started')
        break;
      case 'recordStopped':
        this.emit('record_stopped')
        break;
      case 'iceCandidate':
        this.webRtcPeer.addIceCandidate(parsedMessage.candidate)
        break;
      default:
        console.error('Unrecognized message', parsedMessage);
    }
  }

  play() {
    throw 'Need to override';
  }

  dispatchVideoUrls(videoUrls: any[]) {
    this.emit('video_urls', videoUrls)
  }

  dispatchSources(sources: any[]) {
    this.sources = sources;
    this.emit('sources', sources)
  }

  onOffer ( error: any, offerSdp: any ) {
    if (error)
      return this.onError(error);
    // console.log(this.options.type)
    var message = {
      event : this.options.type,
      sdpOffer : offerSdp
    };
    this.sendMessage(message);
  } 

  response (message: any) {
    if (message.response != 'accepted') {
      var errorMsg = message.message ? message.message : 'Unknow error';
      console.warn('Call not accepted for the following reason: ' + errorMsg);
      this.reconnect();
    } else {
      this.webRtcPeer.processAnswer(message.sdpAnswer);
    }
  }

  sendMessage ( message: any ) {
    if(this.options.roomID) {
      message.room = this.options.roomID;
    }
    var jsonMessage = JSON.stringify({
      event: message.event,
      data: message
    });

    console.log('Senging message: ' + jsonMessage);
    this.ws.send(jsonMessage);
  }

  onIceCandidate(candidate: any) {
    console.log('Local candidate' + JSON.stringify(candidate));

    var message = {
      event: 'onIceCandidate',
      candidate: candidate
    }
    this.sendMessage(message);
  }

  showSpinner(el: any) {
    el.poster = './img/transparent-1px.png';
    el.style.background = 'center transparent url("./img/spinner.gif") no-repeat';
  }

  hideSpinner(el: any) {
    el.src = '';
    el.poster = './img/webrtc.png';
    el.style.background = '';
  }

  dispose(){
    console.log("DISPOOSE"); //TODO реализовать

    if (this.webRtcPeer) {
      this.webRtcPeer.dispose();
      this.webRtcPeer = null;
    }
    this.hideSpinner(this.video);
  }

  stop(){
    this.emit('stop')
    this.destroy()
  }

  destroy() {
    this.ws.close();
    this.ws = null;
    if (this.webRtcPeer) {
      // var message = {
      //   event : 'stop'
      // }
      // this.sendMessage(message);
      this.dispose();
    }
  }

  onError(e: any){
    this.emit('error', e)
    console.error("bug", e);
  }

  startRecord(){
      var message = {
          event : 'startRecord'
      }
      this.sendMessage(message);
  }

  stopRecord() {
      var message = {
          event: 'stopRecord'
      }
      this.sendMessage(message);
  }

  setSource(id: string) {
      var message = {
          event : 'setSource',
          source:id
      }
      this.sendMessage(message);
  }

  addSource(id: string) {
      var message = {
          event: 'addSource',
          source:id
      }
      this.sendMessage(message);
  }

  stopVideo(id: string){
      var message = {
          event: 'stopVideo',
          source: id
      }
      this.sendMessage(message);
  }   

  startVideo(uri: string){
      var message = {
          event: 'startVideo',
          uri: uri
      }
      this.sendMessage(message);
  }  

  reconnect() {
    this.destroy();
    console.log('reconnect')
    if(this.reconnectCounter == 10) {
      return;
    }
    setTimeout(() => {
      this.reconnectCounter++;
      this.timeReconnect += 5000;
      this.play()
    }, this.timeReconnect)
  }

}