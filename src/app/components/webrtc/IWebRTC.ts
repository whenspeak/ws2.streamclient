import * as EventEmitter from 'eventemitter3';

export interface IWebRTC extends EventEmitter {
	ws: any;
	webRtcPeer: any;
	el: any;
	video: any;
    play(): void;
    stop(): void;
    startRecord(): void;
    stopRecord(): void;
    setSource(id: string): void;
    addSource(id: string): void;
    startVideo(uri: string): void;
    stopVideo(id: string): void;
    reconnect(): void;
}