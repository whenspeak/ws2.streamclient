import { WebRtcPeer } from 'kurento-utils';
import { WebRTC } from './webrtc';
import { IConfig } from '../IConfig';
import * as stun from '../../../../node_modules/freeice/stun.json';

export class Recorder extends WebRTC {

  constructor(config: IConfig) {
    super(config)
  }

  play() {
    if (!this.webRtcPeer) {
      this.showSpinner(this.video);

      var options = {
        localVideo: this.video,
        onicecandidate : this.onIceCandidate,
        videoStream: this.options.videoStream,
        mediaConstraints: this.options.mediaConstraints,
        configuration: {
         iceServers: [{
          url: "turn:93.95.98.89:3478",
          username: "whenspeak",
          credential: "whenspeak"           
         }, {
           url: "stun:"+stun[0],
           urls: ["stun:"+stun[0]]
         }, {
           url: "stun:"+stun[1],
           urls: ["stun:"+stun[1]]
         }]
        }

      }
      this.ws = new WebSocket(this.options.server);
      this.ws.onmessage = this.onMessage;
      this.ws.onopen = () => {
        this.emit('open_ws')    
        const self = this        
        this.webRtcPeer = WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
          if(error)
            return self.onError(error);

          this.generateOffer( self.onOffer );
        });     
      }      
    }

    this.ws.onerror = () => {
      this.reconnect()  
    }
  } 
    
}