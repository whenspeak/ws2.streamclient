import { WebRtcPeer } from 'kurento-utils';
import { WebRTC } from './webrtc';
import { IConfig } from '../IConfig';
import * as UAParser from 'ua-parser-js';
import * as stun from '../../../../node_modules/freeice/stun.json';

export class Player extends WebRTC {

  constructor(config: IConfig) {
    super(config)
  }

  play() {
    if (this.webRtcPeer)
      return;

    this.showSpinner(this.video);

    var options = {
      remoteVideo: this.video,
      onicecandidate: this.onIceCandidate,
      configuration: {
       iceServers: [{
        url: "turn:93.95.98.89:3478",
        username: "whenspeak",
        credential: "whenspeak"           
       }, {
         url: "stun:"+stun[0],
         urls: ["stun:"+stun[0]]
       }, {
         url: "stun:"+stun[1],
         urls: ["stun:"+stun[1]]        
       }]
      }
    }
    this.ws = new WebSocket(this.options.server);
    this.ws.onmessage = this.onMessage;
    this.ws.onopen = () => {
      this.emit('open_ws')    
      const safari = (new UAParser()).getBrowser().name.toLowerCase() == 'safari' ? true : false;
      const WebRtcPeerSend = safari ?  WebRtcPeer.WebRtcPeerSendrecv : WebRtcPeer.WebRtcPeerRecvonly;
      const self = this
      this.webRtcPeer = WebRtcPeerSend(options, function(error: any) {
        if (error)
          return this.onError(error);
        this.generateOffer(self.onOffer);
      });      
    }

    this.ws.onerror = () => {
      this.reconnect()  
    }

  }

}