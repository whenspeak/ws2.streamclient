export interface IConfig {
		type: string;
		server: string;
		element: HTMLElement;
		roomID: string; //presenter || viewer,
		style: {
			button: string;
			buttonHover: string;
		};
		autoplay: boolean;
		videoStream: MediaStream;
		mediaConstraints: MediaStreamConstraints;
		onPlaying(): void;
		onError(): void;
		onGetSources(sources: any[]): void;
		onGetRecordUris(uris: any[]): void;
    // onOffer(error: any, offerSdp: any): void;
}