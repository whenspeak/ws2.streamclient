function request(url: string, timeout: number) {
  return new Promise((resolve, reject) => {
    let win = window, doc = document, el = 'script'

    let onLoad = (r: any) => {
      win.clearTimeout(timer)
      console.log('onlaod')
      if (r)
        resolve()
      else
        reject()
    }

    let timer: number = null
    let first = doc.getElementsByTagName(el)[0]
    let script: any = doc.createElement(el)

    script.src = url
    script.async = true
    script.onload = () => { onLoad(true) }
    script.onerror = () => { onLoad(false) }
    first.parentNode.insertBefore(script, first)
    if (timeout) {
      timer = win.setTimeout(() => {
        console.log(url)
        onLoad(false)
      }, timeout)
    }
  })
}


export async function load(urls:string[]) {
  await Promise.all(urls.map(
   async url => {
    const content = await request(url, 4000);
   })
  );
}