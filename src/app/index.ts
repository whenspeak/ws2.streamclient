// import * as React from 'react';
// import * as ReactDOM from 'react-dom';
import { Player } from './components/webrtc/player';
import { Recorder } from './components/webrtc/recorder';
import { IWebRTC } from './components/webrtc/IWebRTC';
import { IConfig } from './components/IConfig';
import { load } from './utils/scriptLoader';
import { Skin } from './skin/index';
import { video } from '../canAutoplay/index';

export default class PlayerWidget {
	public player: IWebRTC;
	private libraries: string[] = ['https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/6.4.0/adapter.min.js']
	private defaultOptions: any = {
		type: 'viewer',
		server: 'ws://localhost:9080',
		roomID: '10',
		autoplay: true,
		style: {
			button: 'white',
			buttonHover: '#f28b00'
		}
	};
	constructor(private options: IConfig) {
		this.options = Object.assign(this.defaultOptions, this.options)
		this.init()

	}

	async init() {
		await load(this.libraries)		
		let resAutoplay: any = await video();
		if(this.options.autoplay) {
			if(!resAutoplay.result) {
				this.options.autoplay = false;
			} else {
				this.options.autoplay = true;
			}			
		}


		// this.options.autoplay = false;
		if(this.options.type == 'presenter') {
			this.player = new Recorder(this.options)
		} else {
			this.player = new Player(this.options)
		}

		var skin = new Skin(this.options, this.player);

		if(this.options.autoplay) this.player.play()

		this.player.on('sources', (e) => {
			if(this.options.onGetSources) {
				this.options.onGetSources(e)
			}			
		})		
		this.player.on('video_urls', (e) => {
			if(this.options.onGetRecordUris) {
				this.options.onGetRecordUris(e)
			}			
		})				
		this.player.once('playing', () => {
			if(this.options.onPlaying) {
				this.options.onPlaying()
			}
		})
		this.player.once('error', () => {
			if(this.options.onError) {
				this.options.onError()
			}
		});
		
	}

	stop() {
		if(!this.player) return
		this.player.stop()	
	}
}
