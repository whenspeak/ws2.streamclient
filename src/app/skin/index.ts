import * as playIcon from './icons/play-button.svg';
import * as fullscreenIcon from './icons/fullscreen.svg';
import * as publishIcon from './icons/power-button.svg';
import * as stopIcon from './icons/stop.svg';
import * as html from './html/index.html';
import './scss/index.scss';
import { IConfig } from '../components/IConfig';
import { toggleElement } from '../utils/utils';
import { IWebRTC } from '../components/webrtc/IWebRTC';
import * as UAParser from 'ua-parser-js';

export class Skin {
	public publishButton: HTMLElement;
	public playButton: HTMLElement;
	public stopButton: HTMLElement;
	public fullscreenButton: HTMLElement;
	private bigPlayButton: HTMLElement;
	private controlPanel: HTMLElement;
	private area: HTMLElement;
	private errorScreen: HTMLElement;
	private errorText: HTMLElement;	
	public video: HTMLVideoElement;
	private fullscreen: boolean;
	private records: HTMLElement;

	constructor(public options: IConfig, public player: IWebRTC) {

		this.initialize()
	}

	initialize() {
		console.log('initialize')
	    let skinContainer: HTMLElement = document.createElement("div");
	    skinContainer.className = 'wspeak-skin';
	    this.options.element.appendChild(skinContainer)
	    skinContainer.innerHTML = html;
	    
	    this.controlPanel = skinContainer.querySelector('.wspeak-control-panel');
	    this.area = skinContainer.querySelector('.wspeak-area');
	    this.publishButton = skinContainer.querySelector('.wspeak-publish-button');
	    this.playButton = skinContainer.querySelector('.wspeak-play-button');
	    this.stopButton = skinContainer.querySelector('.wspeak-stop-button');
	    this.fullscreenButton = skinContainer.querySelector('.wspeak-fs-button');
	    this.bigPlayButton = skinContainer.querySelector('.wspeak-big-play-icon');
	    this.errorScreen = skinContainer.querySelector('.wspeak-error-screen');
	    this.errorText = skinContainer.querySelector('.wspeak-error-text');	    
	   	this.records = this.options.element.querySelector('.wspeak-record')
	    this.video = this.options.element.querySelector('video')

	    const autoplay = this.options.autoplay
	    this.player.on('record_started', () => {
			this.removeClass(this.records, 'hide')
			this.addClass(this.records, 'show')
	    })
	    this.player.on('record_stopped', () => {
			this.addClass(this.records, 'hide')
			this.removeClass(this.records, 'show')	    	
	    })	
	    this.player.on('playing', (data) => {
	    	this.errorScreen.style.display = 'none'
	    })
	    this.player.on('error', (data) => {
	    	toggleElement(this.controlPanel)
	    	if(data.message) {
	    		this.errorText.innerHTML = data.message
	    	} else {
	    		this.errorText.innerHTML = 'Ошибка'
	    	}
	    	this.errorScreen.style.display = 'flex'
	    })    
	    if(autoplay) {
	    	toggleElement(this.playButton)
	    	toggleElement(this.publishButton)
	    	this.controlPanel.style.display = 'block'
	    	this.area.style.display = 'none'
	    } else {
	    	toggleElement(this.stopButton)
	    	this.controlPanel.style.display = 'none'
	    	this.area.style.display = 'flex'	    	
	    	// toggleElement(this.playButton)
	    	// this.stopButton.style.display = 'none'
	    }
	    if(this.options.type == 'presenter') {
	    	this.playButton.remove()
	    } else {
	    	this.publishButton.remove()
	    }
	    this.render()
	    this.events();
	    this.customStyle()
	}


	customStyle() {
		if(!this.options.style) return
		// if(!this.options.bu)
		var css: string = `.wspeak-skin  .wspeak-button svg path { background: ${this.options.style.button}!important; }` 
		+ `.wspeak-skin .wspeak-button:hover svg path { background: ${this.options.style.buttonHover}!important; }`,
		    head: HTMLHeadElement = document.head || document.getElementsByTagName('head')[0],
		    style: any = document.createElement('style');

		style.type = 'text/css';
		if (style.styleSheet){
		  // This is required for IE8 and below.
		  style.styleSheet.cssText = css;
		} else {
		  style.appendChild(document.createTextNode(css));
		}

		head.appendChild(style);		
	}

	hasClass(el:HTMLElement, className:string)
	{
	    if (el.classList)
	        return el.classList.contains(className);
	    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
	}	

	addClass(el:HTMLElement, className:string)
	{
	    if (el.classList)
	        el.classList.add(className)
	    else if (!this.hasClass(el, className))
	        el.className += " " + className;
	}

	removeClass(el:HTMLElement, className:string)
	{
	    if (el.classList)
	        el.classList.remove(className)
	    else if (this.hasClass(el, className))
	    {
	        var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
	        el.className = el.className.replace(reg, ' ');
	    }
	}	

	events() {
		this.playButton.onclick = () => {
			this.video.setAttribute("autoplay", "");  
			this.player.play()
			this.toggle()
		}
		this.publishButton.onclick = () => {
			this.player.play()
			this.toggle()
		}		
		this.stopButton.onclick = () => {
			this.player.stop()
			this.toggle()
		}	
		this.fullscreenButton.onclick = () => {
			this.toggleFullscreen()
		}
		this.area.onclick = () => {
			this.video.setAttribute("autoplay", "");  
			this.player.play()	
			this.toggle()		
	    	this.controlPanel.style.display = 'block'
	    	this.area.style.display = 'none'			
		}

		document.addEventListener("fullscreenchange", this.onFullScreenChange.bind(this));
		document.addEventListener("webkitfullscreenchange", this.onFullScreenChange.bind(this));
		document.addEventListener("mozfullscreenchange", this.onFullScreenChange.bind(this));
		const ios = (new UAParser()).getOS().name.toLowerCase() == 'ios';
		if(ios) {
			this.video.addEventListener('webkitendfullscreen', () => {
				this.fullscreen = false;
			})
		}
	}

	onFullScreenChange() {
		const doc: any = document
		const fullscreenElement = doc.fullscreenElement || doc.mozFullScreenElement || doc.webkitFullscreenElement;
		this.fullscreen = !!fullscreenElement
	}	

	toggleFullscreen() {
		if(!this.fullscreen) {
			let elem: any = this.options.element
			if (elem.requestFullscreen) {
				elem.requestFullscreen();
			} else if (elem.mozRequestFullScreen) { /* Firefox */
				elem.mozRequestFullScreen();
			} else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
				elem.webkitRequestFullscreen();
			} else if (elem.msRequestFullscreen) { /* IE/Edge */
				elem.msRequestFullscreen();
			}			
		} else {
			let doc: any = document
			if (doc.exitFullscreen) {
				doc.exitFullscreen();
			} else if (doc.mozCancelFullScreen) { /* Firefox */
				doc.mozCancelFullScreen();
			} else if (doc.webkitExitFullscreen) { /* Chrome, Safari and Opera */
				doc.webkitExitFullscreen();
			} else if (doc.msExitFullscreen) { /* IE/Edge */
				doc.msExitFullscreen();
			}			
		}
	}

	toggle() {
		toggleElement(this.publishButton)
		toggleElement(this.playButton)	
	   	toggleElement(this.stopButton)			
	}

	render() {
		this.publishButton.innerHTML = publishIcon;
		this.playButton.innerHTML = playIcon;
		this.stopButton.innerHTML = stopIcon;
		this.fullscreenButton.innerHTML = fullscreenIcon;
		this.bigPlayButton.innerHTML = playIcon
	}

}