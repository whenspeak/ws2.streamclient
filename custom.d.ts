declare module "*.svg" {
  const content: string;
  export = content;
}

declare module "*.html" {
  const content: string;
  export = content;
}